import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'blogAngular';

  Posts = [
    {
      title: "Mon premier Post",
      content:" Iam in altera philosophiae parte. quae est quaerendi ac disserendi, quae logikh dicitur, iste vester plane,",
      loveIts: 0,
      createdAt: new Date,
    },
    { 
      title: "Mon deuxieme Post",
      content:" Iam in altera philosophiae parte. quae est quaerendi ac disserendi, quae logikh dicitur, iste vester plane,",
      loveIts: 0,
      createdAt: new Date,
    },
    {
      title: "Mon troisieme Post",
      content:" Iam in altera philosophiae parte. quae est quaerendi ac disserendi, quae logikh dicitur, iste vester plane,",
      loveIts: 0,
      createdAt: new Date,
    }
  ]

  
}
